
export class Table {
  constructor(tableID,data) {
  	this.tableID = document.getElementById(tableID);
    this.columns = data.columns;
    this.data = data.data;
  }

  createTable(data){
   		//buat tabel
   		let table = document.createElement('table');

   		//buat baris header
   		let headerRow = document.createElement('tr');
      headerRow.classList.add('table');
   		for(let v of this.columns){
   			let headerCell = document.createElement('th');
   			headerCell.appendChild(document.createTextNode(v));
   			headerRow.appendChild(headerCell);
   		}
   		table.appendChild(headerRow);
    
    for(let dataPerson of this.data){
      console.log(dataPerson,"<<<");
      let dataRow = document.createElement('tr');
      for(let person of dataPerson){
        console.log(person,">>>>");
        let dataCell = document.createElement('td');
        let dataText = document.createTextNode(person);
        dataCell.appendChild(dataText);
        dataRow.appendChild(dataCell);
      }
      table.appendChild(dataRow);
    }

   this.tableID.appendChild(table);
  }
}


// let data = { 
//      columns: ['Name', 'Email', 'Phone Number'],
//      data:   [],
//      };



// let inputName = prompt('Masukan Nama Anda : ') || "-";
// let inputEmail = prompt('Masukan Email Anda : ') || "-";
// let inputPhoneNumber = prompt('Masukan Nomer Ponsel Anda : ') || "-";

// data.data.push([inputName, inputEmail, inputPhoneNumber]);
// let inputAgain = confirm("Mau menambahkan data lagi ?");
// while(inputAgain){
//     inputName = prompt('Masukan Nama Anda : ') || "-";
//     inputEmail = prompt('Masukan Email Anda : ') || "-";
//     inputPhoneNumber = prompt('Masukan Nomer Ponsel Anda : ') || "-";
//     data.data.push([inputName, inputEmail, inputPhoneNumber]);
//     inputAgain = confirm("Mau menambahkan data lagi ?");
// }







// let table = new Table("myTable", data);
// table.createTable(data);



// let data = { 
//      columns: ['Name', 'Email', 'Phone Number'],
//      data:   [
//          ['Bangkit', 'bangkit21@gmail.com', '083870952303'],
//          ['Setio', 'setio21@gmail.com', '08666666633']
//          ],
//      };
